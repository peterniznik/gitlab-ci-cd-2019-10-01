#!/usr/bin/env bash

mkdir -p ~/.ssh
chmod 700 ~/.ssh

echo "$PRIVATE_SSH_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

ssh -o StrictHostKeyChecking=no root@gitlab.janmikes.cz 'cd /app && sh deploy-honza.sh'
